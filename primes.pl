#!/usr/bin/env perl

use strict;
use warnings;

use Math::Prime::XS qw(is_prime);
use DBI;
use DateTime;
use Data::Dumper;
use File::Basename qw(basename);
use FindBin;
use Config::Tiny;
use Time::HiRes qw(usleep);
use Net::Twitter;

my $cfg = Config::Tiny->read('primes.conf') || Config::Tiny::errstr();
my $DEBUG = $cfg->{main}->{debug} || 0;
my $module = $cfg->{twitter}->{module} || 0;

my $appname = basename($0);
$appname =~ s/\.pl$//;
my $twitter_log = '/var/log/twitter/tw_primes.log';
my $db_file = 'primes.db';

my $dt = DateTime->now(time_zone => 'Europe/Rome');

my $dbh = DBI->connect("dbi:SQLite:dbname=$db_file") or die $DBI::errstr;
mylog("Opened database successfully")
    if $DEBUG;
    
my $nt = Net::Twitter->new(
    ssl                 => 1,
    traits              => [qw/API::RESTv1_1/],
    consumer_key        => $cfg->{twitter}->{tw_api_key},
    consumer_secret     => $cfg->{twitter}->{tw_api_key_secret},
    access_token        => $cfg->{twitter}->{tw_access_token},
    access_token_secret => $cfg->{twitter}->{tw_access_token_secret},
) if $cfg->{twitter}->{do_tweet};

my $sth = $dbh->prepare("SELECT count(*) AS cnt FROM primes");
my $rv = $sth->execute() or die $DBI::errstr;
my $row = $sth->fetchrow_hashref();
if ($row->{cnt} < 2) {
    resetDB();
    insertIntoDB(1,2);
    insertIntoDB(2,3);
} 

foreach my $step (1..$ARGV[0] || 1) {
    $row = getNextPrimeOnDB();
    if ($row) {
        if ($module and
            $dt->minute % $module != $row->{prime} % $module
        ) {
            mylog(sprintf "Use key %s on DB but modular is %s and minutes is %s",
                            $row->{id}, $row->{prime} % $module, $dt->minute
                 );
            exit 0;
        }
        mylog("Use key $row->{id} -> $row->{prime} on DB");
        printf "\nid = %s\np = %s", $row->{id}, $row->{prime}
            if $DEBUG;
        doTweet($row->{prime})
            if $cfg->{twitter}->{do_tweet};
        my $sql = qq/UPDATE primes SET dt_used = datetime('now') WHERE id = '$row->{id}'/;
        my $rv = $dbh->do($sql) or die $DBI::errstr;
    } else {
        my $sql = qq(SELECT id, prime
                     FROM primes
                     ORDER BY id DESC
                     LIMIT 1
        );
        $sth = $dbh->prepare($sql);
        $rv = $sth->execute() or die $DBI::errstr;
        $row = $sth->fetchrow_hashref();
    }
    if (checkExistsNextPrime()) {
        printf "\nNon devo calcolare il successivo"
            if $DEBUG;
        exit 0;
    }

    insertIntoDB($row->{id}+1,
                 getNextPrime($row->{prime})
                );
    usleep 100_000;
}
exit 0;

sub mylog {
    my $text = shift;
    my $dt = DateTime->now();
    printf "\n%s [%s] %s", $dt->datetime(q{ }), $appname, $text
        if $DEBUG;
    open FH, '+>>', $twitter_log or die "Can't open > $twitter_log: $!";
;
    printf FH "%s [%s] %s\n", $dt->datetime(q{ }), $appname, $text;
    close FH;
    return $text;
}

sub checkExistsNextPrime {
    my $sql = qq(SELECT count(*) AS cnt
                 FROM primes
                 WHERE dt_used IS NULL
    );
    $sth = $dbh->prepare( $sql );
    $rv = $sth->execute() or die $DBI::errstr;
    if ($rv < 0) {
       print $DBI::errstr;
    }
    return ($sth->fetchrow_hashref()->{cnt} > 0)
        ? 1
        : 0;
}

sub doTweet {
    $nt->update(shift);
    mylog("Do tweet");
    return 0;
}

sub getNextPrimeOnDB {
    my $sql = qq(SELECT id, prime
                 FROM primes
                 WHERE dt_used IS NULL
                 ORDER BY id ASC
                 LIMIT 1;
    );
    $sth = $dbh->prepare( $sql );
    $rv = $sth->execute() or die $DBI::errstr;
    if ($rv < 0) {
       print $DBI::errstr;
    }
    return $sth->fetchrow_hashref();
}

sub insertIntoDB {
    my ($id, $prime)= @_;
    my $sql = qq(INSERT INTO primes (id, prime, dt) VALUES ($id, $prime, datetime('now')));
    my $rv = $dbh->do($sql) or die $DBI::errstr;
    my $msg = "Records $id created successfully";
    mylog($msg);
    printf "\n$msg"
        if $DEBUG;
    return 0;
}

sub getNextPrime {
    my $i = shift;
    do {
        $i += 2;
    } while !is_prime($i);
    return $i;
}

sub resetDB {
    my $sql = qq(DELETE FROM primes);
    my $rv = $dbh->do($sql) or die $DBI::errstr;
    return 0;
}
