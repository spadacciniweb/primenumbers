#!/usr/bin/env perl

use strict;
use warnings;

use DBI;

my $db_file = 'primes.db';
my $dbh = DBI->connect("dbi:SQLite:dbname=$db_file") or die $DBI::errstr;

print "Opened database successfully\n";
my $stmt = qq(CREATE TABLE primes (
    id      INT PRIMARY KEY NOT NULL,
    prime   TEXT NOT NULL,
    dt      TEXR NOT NULL,
    dt_used TEXT
);
);

my $rv = $dbh->do($stmt);
if($rv < 0) {
   print $DBI::errstr;
} else {
   print "Table created successfully\n";
}
$dbh->disconnect();
