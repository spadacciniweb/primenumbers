# PrimeNumbers

You can follow the bot at:
* Twitter: [@math\_numbers](https://twitter.com/math_numbers)
* GitLab: [primenumbers](https://gitlab.com/spadacciniweb/primenumbers)

## Configuration

```
$ sh installdeps_0.1.sh
DBI is up to date. (1.643)
DBD::SQLite is up to date. (1.72)
Math::Prime::XS is up to date. (0.27)

$ cp primes.conf_sample primes.conf
```
and edit the configuration file _primes.conf_ (set `[twitter]` section as needed)

## Create local DB

```
$ perl create_db.pl
Opened database successfully
Table created successfully
```

and check local db:
```
$ sqlite3 primes.db
SQLite version 3.31.1 2020-01-27 19:55:54
Enter ".help" for usage hints.
sqlite> .tables
primes
```

## Usage

### single step (default)
```
$ perl primes.pl 

2022-12-05 10:59:35 [primes] Opened database successfully
2022-12-05 10:59:35 [primes] Records 1 created successfully
Records 1 created successfully
2022-12-05 10:59:35 [primes] Records 2 created successfully
Records 2 created successfully
2022-12-05 10:59:35 [primes] Use key 1 -> 2 on DB
id = 1
p = 2
I have not to calculate the next one
```

and check local db:
```
sqlite> select * from primes;
1|2|2022-12-05 10:59:35|2022-12-05 10:59:35
2|3|2022-12-05 10:59:35|
```

### Multi step (for internal debugging purpose)
```
$ perl primes.pl 10

2022-12-05 11:02:42 [primes] Opened database successfully
2022-12-05 11:02:42 [primes] Use key 2 -> 3 on DB
id = 2
p = 3
2022-12-05 11:02:42 [primes] Records 3 created successfully
Records 3 created successfully
2022-12-05 11:02:42 [primes] Use key 3 -> 5 on DB
id = 3
p = 5
2022-12-05 11:02:42 [primes] Records 4 created successfully
Records 4 created successfully
2022-12-05 11:02:42 [primes] Use key 4 -> 7 on DB
id = 4
p = 7
2022-12-05 11:02:42 [primes] Records 5 created successfully
Records 5 created successfully
2022-12-05 11:02:42 [primes] Use key 5 -> 11 on DB
id = 5
p = 11
2022-12-05 11:02:42 [primes] Records 6 created successfully
Records 6 created successfully
2022-12-05 11:02:42 [primes] Use key 6 -> 13 on DB
id = 6
p = 13
2022-12-05 11:02:42 [primes] Records 7 created successfully
Records 7 created successfully
2022-12-05 11:02:42 [primes] Use key 7 -> 17 on DB
id = 7
p = 17
2022-12-05 11:02:42 [primes] Records 8 created successfully
Records 8 created successfully
2022-12-05 11:02:42 [primes] Use key 8 -> 19 on DB
id = 8
p = 19
2022-12-05 11:02:42 [primes] Records 9 created successfully
Records 9 created successfully
2022-12-05 11:02:42 [primes] Use key 9 -> 23 on DB
id = 9
p = 23
2022-12-05 11:02:42 [primes] Records 10 created successfully
Records 10 created successfully
2022-12-05 11:02:42 [primes] Use key 10 -> 29 on DB
id = 10
p = 29
2022-12-05 11:02:43 [primes] Records 11 created successfully
Records 11 created successfully
2022-12-05 11:02:43 [primes] Use key 11 -> 31 on DB
id = 11
p = 31
2022-12-05 11:02:43 [primes] Records 12 created successfully
Records 12 created successfully
```

and check local db:
```
sqlite> select * from primes;
1|2|2022-12-05 10:59:35|2022-12-05 10:59:35
2|3|2022-12-05 10:59:35|2022-12-05 11:02:42
3|5|2022-12-05 11:02:42|2022-12-05 11:02:42
4|7|2022-12-05 11:02:42|2022-12-05 11:02:42
5|11|2022-12-05 11:02:42|2022-12-05 11:02:42
6|13|2022-12-05 11:02:42|2022-12-05 11:02:42
7|17|2022-12-05 11:02:42|2022-12-05 11:02:42
8|19|2022-12-05 11:02:42|2022-12-05 11:02:42
9|23|2022-12-05 11:02:42|2022-12-05 11:02:42
10|29|2022-12-05 11:02:42|2022-12-05 11:02:42
11|31|2022-12-05 11:02:43|2022-12-05 11:02:43
12|37|2022-12-05 11:02:43|
```


## Authors and acknowledgment
Mariano Spadaccini <spadacciniweb [at] gmail.com>

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

## Project status
Work in progress.
